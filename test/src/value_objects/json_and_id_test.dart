import 'package:abstract_reactive_database/src/types.dart';
import 'package:abstract_reactive_database/src/value_objects/json_and_id.dart';
import 'package:test/test.dart';

void main() {
  group("JSONAndId", () {
    test(
        'it should not allow editing the original JSON passed in the constructor',
        () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSONAndId jsonAndId = JSONAndId(id: 'id', json: json);

      // Act
      json['a'] = 3;

      //Assert
      expect(jsonAndId.json['a'], 1);
    });

    test("Two JSONAndId with the same data and id should be equal", () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSON json2 = <String, dynamic>{'a': 1, 'b': 2};
      final JSONAndId jsonAndId1 = JSONAndId(id: 'id', json: json);
      final JSONAndId jsonAndId2 = JSONAndId(id: 'id', json: json2);

      // Assert
      expect(jsonAndId1, jsonAndId2);
    });

    test("Two JSONAndId with different data should not be equal", () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSON json2 = <String, dynamic>{'a': 1, 'b': 3};
      final JSONAndId jsonAndId1 = JSONAndId(id: 'id', json: json);
      final JSONAndId jsonAndId2 = JSONAndId(id: 'id', json: json2);

      // Assert
      expect(jsonAndId1, isNot(jsonAndId2));
    });

    test("Two JSONAndId with same data and id should have the same hashcode",
        () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSON json2 = <String, dynamic>{'a': 1, 'b': 2};
      final JSONAndId jsonAndId1 = JSONAndId(id: 'id', json: json);
      final JSONAndId jsonAndId2 = JSONAndId(id: 'id', json: json2);

      // Assert
      expect(jsonAndId1.hashCode, jsonAndId2.hashCode);
    });

    test("Two JSONAndId with different data should not have the same hashcode",
        () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSON json2 = <String, dynamic>{'a': 1, 'b': 3};
      final JSONAndId jsonAndId1 = JSONAndId(id: 'id', json: json);
      final JSONAndId jsonAndId2 = JSONAndId(id: 'id', json: json2);

      // Assert
      expect(jsonAndId1.hashCode, isNot(jsonAndId2.hashCode));
    });

    test("Two JSONAndId with different id should not have the same hashcode",
        () {
      // Arrange
      final JSON json = <String, dynamic>{'a': 1, 'b': 2};
      final JSON json2 = <String, dynamic>{'a': 1, 'b': 2};
      final JSONAndId jsonAndId1 = JSONAndId(id: 'id', json: json);
      final JSONAndId jsonAndId2 = JSONAndId(id: 'id2', json: json2);

      // Assert
      expect(jsonAndId1.hashCode, isNot(jsonAndId2.hashCode));
    });
  });
}
