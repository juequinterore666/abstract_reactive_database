part of write_date_tracker_reactive_database_test;

void deleteWhere() {
  test(
    'should pass call to received database',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );

      final List<IQueryClause> queryClauses = <IQueryClause>[
        IQueryClause.where(
            comparator: IWhereComparator.lessThanOrEquals(value: 44),
            field: "age"),
        IQueryClause.where(
            comparator: IWhereComparator.equals(value: 25), field: "size")
      ];

      // Act
      writeDateTrackerReactiveDatabase.deleteWhere(
        table: 'table',
        queryClauses: queryClauses,
      );

      // Assert
      verify(
        reactiveDatabase.deleteWhere(
          table: 'table',
          queryClauses: queryClauses,
        ),
      );
    },
  );
}
