part of write_date_tracker_reactive_database_test;

void delete() {
  test(
    'should pass call to received database',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );

      final List<String> ids = <String>['id', 'id2'];

      // Act
      writeDateTrackerReactiveDatabase.delete(
        table: 'table',
        ids: ids,
      );

      // Assert
      verify(
        reactiveDatabase.delete(
          table: 'table',
          ids: ids,
        ),
      );
    },
  );
}
