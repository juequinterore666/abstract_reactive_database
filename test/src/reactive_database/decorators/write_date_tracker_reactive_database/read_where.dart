part of write_date_tracker_reactive_database_test;

void readWhere() {
  test(
    'should pass call to received database',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final List<IQueryClause> queryClauses = <IQueryClause>[
        IQueryClause.where(
          field: 'field',
          comparator: IWhereComparator.equals(value: 'value'),
        ),
        IQueryClause.sort(order: SortQueryClauseOrder.asc, field: 'age')
      ];

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      when(reactiveDatabase.readWhere(
              table: 'table', queryClauses: queryClauses))
          .thenAnswer((_) =>
              Stream<PaginatedJSONs>.value(PaginatedJSONs(documents: <JSON>[
                <String, dynamic>{'a': 1, 'b': 2},
                <String, dynamic>{'a': 3, 'b': 4}
              ], nextSkip: null)));
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );

      // Act
      writeDateTrackerReactiveDatabase.readWhere(
        table: 'table',
        queryClauses: queryClauses,
      );

      // Assert
      verify(
        reactiveDatabase.readWhere(
          table: 'table',
          queryClauses: queryClauses,
        ),
      );
    },
  );
}
