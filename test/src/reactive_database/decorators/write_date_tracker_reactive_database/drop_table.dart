part of write_date_tracker_reactive_database_test;

void dropTable() {
  test(
    'should pass call to received database',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );

      // Act
      writeDateTrackerReactiveDatabase.dropTable(
        table: 'table',
      );

      // Assert
      verify(
        reactiveDatabase.dropTable(
          table: 'table',
        ),
      );
    },
  );
}
