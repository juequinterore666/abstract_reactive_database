part of write_date_tracker_reactive_database_test;

void read() {
  test(
    'should pass call to received database',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      when(reactiveDatabase.read(
        table: 'table',
        id: 'id',
      )).thenAnswer((_) => Stream<JSON>.value(
            <String, dynamic>{'a': 1, 'b': 2},
          ));
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );

      // Act
      writeDateTrackerReactiveDatabase.read(
        table: 'table',
        id: 'id',
      );

      // Assert
      verify(
        reactiveDatabase.read(
          table: 'table',
          id: 'id',
        ),
      );
    },
  );
}
