part of write_date_tracker_reactive_database_test;

void runTransaction() {
  test('should pass call to received database', () {
    // Arrange
    final DateTime testDate = DateTime.now();
    final IDateProvider dateProvider = MockIDateProvider();
    when(dateProvider.millisecondsSinceEpoch())
        .thenReturn(testDate.millisecondsSinceEpoch);

    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();

    when(
      reactiveDatabase.runTransaction(any),
    ).thenAnswer(
      (_) async {},
    );

    final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
        WriteDateTrackerReactiveDatabase(
      reactiveDatabase,
      dateProvider,
    );

    Future<void> transactionHandler(IReactiveDatabase transaction) async {}

    // Act
    writeDateTrackerReactiveDatabase.runTransaction(
      transactionHandler,
    );

    // Assert
    verify(
      reactiveDatabase.runTransaction(
        transactionHandler,
      ),
    );
  });
}
