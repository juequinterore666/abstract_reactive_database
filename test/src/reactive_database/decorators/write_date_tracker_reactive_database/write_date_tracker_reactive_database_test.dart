library write_date_tracker_reactive_database_test;

import 'package:abstract_reactive_database/src/lib.dart';
import 'package:abstract_reactive_database/src/reactive_database/decorators/write_date_tracker_reactive_database.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'write_date_tracker_reactive_database_test.mocks.dart';

part 'create.dart';
part 'delete.dart';
part 'delete_where.dart';
part 'drop_table.dart';
part 'read.dart';
part 'read_where.dart';
part 'run_transaction.dart';
part 'update.dart';
part 'upsert.dart';

@GenerateMocks(<Type>[IReactiveDatabase, IDateProvider])
void main() {
  group("WriteDateTrackerReactiveDatabase", () {
    group("create", () {
      create();
    });
    group("delete", () {
      delete();
    });
    group("deleteWhere", () {
      deleteWhere();
    });
    group("dropTable", () {
      dropTable();
    });
    group("read", () {
      read();
    });
    group("readWhere", () {
      readWhere();
    });
    group("runTransaction", () {
      runTransaction();
    });
    group("update", () {
      update();
    });
    group("upsert", () {
      upsert();
    });
  });
}
