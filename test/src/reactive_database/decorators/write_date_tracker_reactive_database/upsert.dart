part of write_date_tracker_reactive_database_test;

void upsert() {
  test(
    'should pass call to received database adding updateDate and creationDate to json',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );
      final JSONAndId jsonAndId =
          JSONAndId(id: 'id', json: <String, dynamic>{"a": 1, "b": 2});
      final JSONAndId jsonAndIdWithWriteDate = JSONAndId(
        id: 'id',
        json: <String, dynamic>{
          "a": 1,
          "b": 2,
          "updateDate": testDate.millisecondsSinceEpoch,
          "creationDate": testDate.millisecondsSinceEpoch,
        },
      );

      // Act
      writeDateTrackerReactiveDatabase.upsert(
        table: 'table',
        jsonsAndIds: <JSONAndId>[
          jsonAndId,
        ],
      );

      // Assert
      verify(
        reactiveDatabase.upsert(
          table: 'table',
          jsonsAndIds: <JSONAndId>[
            jsonAndIdWithWriteDate,
          ],
        ),
      );
    },
  );

  test(
    'should not modify original references',
    () {
      // Arrange
      final DateTime testDate = DateTime.now();
      final IDateProvider dateProvider = MockIDateProvider();
      when(dateProvider.millisecondsSinceEpoch())
          .thenReturn(testDate.millisecondsSinceEpoch);

      final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
      final WriteDateTrackerReactiveDatabase writeDateTrackerReactiveDatabase =
          WriteDateTrackerReactiveDatabase(
        reactiveDatabase,
        dateProvider,
      );
      final JSONAndId jsonAndId =
          JSONAndId(id: 'id', json: <String, dynamic>{"a": 1, "b": 2});

      // Act
      writeDateTrackerReactiveDatabase.upsert(
        table: 'table',
        jsonsAndIds: <JSONAndId>[
          jsonAndId,
        ],
      );

      // Assert
      expect(jsonAndId.json.containsKey("updateDate"), isFalse);
      expect(jsonAndId.json.containsKey("creationDate"), isFalse);
    },
  );
}
