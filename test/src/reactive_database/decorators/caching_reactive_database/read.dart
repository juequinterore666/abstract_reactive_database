part of caching_reactive_database_test;

void read() {
  test("should return same data from Stream from decorated IReactiveDatabase",
      () async {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final JSON json = <String, dynamic>{"a": 1, "b": 2};
    final Stream<JSON?> stream =
        Stream<JSON?>.fromIterable(<JSON?>[null, json, null]);

    when(
      reactiveDatabase.read(
        table: 'table',
        id: 'id',
      ),
    ).thenAnswer((_) => stream);

    // Act
    final Stream<JSON?> result = cachingReactiveDatabase.read(
      table: 'table',
      id: 'id',
    );

    //Assert
    expect(result, emitsInOrder(<JSON?>[null, json, null]));
  });

  test(
      "should return the same instance if called multiple times with the same id,",
      () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final JSON json = <String, dynamic>{"a": 1, "b": 2};
    final Stream<JSON?> stream =
        Stream<JSON?>.fromIterable(<JSON?>[null, json, null]);

    when(
      reactiveDatabase.read(
        table: 'table',
        id: 'id',
      ),
    ).thenAnswer((_) => stream);

    // Act
    final Stream<JSON?> result1 = cachingReactiveDatabase.read(
      table: 'table',
      id: 'id',
    );
    final Stream<JSON?> result2 = cachingReactiveDatabase.read(
      table: 'table',
      id: 'id',
    );

    //Assert
    expect(result1, result2);
  });
}
