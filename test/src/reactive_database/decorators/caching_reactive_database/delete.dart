part of caching_reactive_database_test;

void delete() {
  test('should pass call to received database', () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final String jsonId1 = 'id';
    final String jsonId2 = 'id2';

    // Act
    cachingReactiveDatabase.delete(
      table: 'table',
      ids: <String>[jsonId1, jsonId2],
    );

    // Assert
    verify(
      reactiveDatabase.delete(
        table: 'table',
        ids: <String>[jsonId1, jsonId2],
      ),
    );
  });
}
