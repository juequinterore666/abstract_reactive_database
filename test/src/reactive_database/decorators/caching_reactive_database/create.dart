part of caching_reactive_database_test;

void create() {
  test('should pass call to received database', () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);
    final JSONAndId jsonAndId =
        JSONAndId(id: 'id', json: <String, dynamic>{"a": 1, "b": 2});
    final JSONAndId jsonAndId2 = JSONAndId(id: 'id2', json: <String, dynamic>{
      "b": 2,
      "c": 3,
    });

    // Act
    cachingReactiveDatabase.create(
      table: 'table',
      jsonsAndIds: <JSONAndId>[jsonAndId, jsonAndId2],
    );

    // Assert
    verify(
      reactiveDatabase.create(
        table: 'table',
        jsonsAndIds: <JSONAndId>[jsonAndId, jsonAndId2],
      ),
    );
  });
}
