part of caching_reactive_database_test;

void runTransaction() {
  test('should pass call to received database', () {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    when(
      reactiveDatabase.runTransaction(any),
    ).thenAnswer(
      (_) async {},
    );
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    Future<void> transactionHandler(IReactiveDatabase transaction) async {}

    // Act
    cachingReactiveDatabase.runTransaction(transactionHandler);

    // Assert
    verify(
      reactiveDatabase.runTransaction(transactionHandler),
    );
  });
}
