part of caching_reactive_database_test;

void readWhere() {
  test("should return same data from Stream from decorated IReactiveDatabase",
      () async {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final JSON json = <String, dynamic>{"age": 21, "children": 1};
    final JSON json2 = <String, dynamic>{"age": 28, "children": 0};
    final PaginatedJSONs paginatedJSONs =
        PaginatedJSONs(documents: <JSON>[json, json2]);

    final Stream<PaginatedJSONs> stream =
        Stream<PaginatedJSONs>.value(paginatedJSONs);

    final IQueryClause queryClause = IQueryClause.where(
        comparator: IWhereComparator.greaterThan(value: 20), field: "age");

    final IQueryClause queryClause2 = IQueryClause.where(
        comparator: IWhereComparator.lessThan(value: 2), field: "children");

    when(
      reactiveDatabase.readWhere(
        table: 'table',
        queryClauses: <IQueryClause>[queryClause, queryClause2],
      ),
    ).thenAnswer((_) => stream);

    // Act
    final Stream<PaginatedJSONs> result = cachingReactiveDatabase.readWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause, queryClause2],
    );

    //Assert
    expect(result, emitsInOrder(<PaginatedJSONs>[paginatedJSONs]));
  });

  test(
      "should return the same instance if called multiple times with with same queries,",
      () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final JSON json = <String, dynamic>{"age": 21, "children": 1};
    final JSON json2 = <String, dynamic>{"age": 28, "children": 0};
    final PaginatedJSONs paginatedJSONs =
        PaginatedJSONs(documents: <JSON>[json, json2]);

    final Stream<PaginatedJSONs> stream =
        Stream<PaginatedJSONs>.value(paginatedJSONs);

    final IQueryClause queryClause = IQueryClause.where(
        comparator: IWhereComparator.greaterThan(value: 20), field: "age");

    final IQueryClause queryClause2 = IQueryClause.where(
        comparator: IWhereComparator.lessThan(value: 2), field: "children");

    when(
      reactiveDatabase.readWhere(
        table: 'table',
        queryClauses: <IQueryClause>[queryClause, queryClause2],
      ),
    ).thenAnswer((_) => stream);

    // Act
    final Stream<PaginatedJSONs> result1 = cachingReactiveDatabase.readWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause, queryClause2],
    );
    final Stream<PaginatedJSONs> result2 = cachingReactiveDatabase.readWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause, queryClause2],
    );

    //Assert
    expect(result1, result2);
  });

  test(
      "should return the different instance if called multiple times with the different queries",
      () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final JSON json = <String, dynamic>{"age": 21, "children": 3};
    final JSON json2 = <String, dynamic>{"age": 28, "children": 1};
    final PaginatedJSONs paginatedJSON1 =
        PaginatedJSONs(documents: <JSON>[json]);

    final PaginatedJSONs paginatedJSON2 =
        PaginatedJSONs(documents: <JSON>[json2]);

    final Stream<PaginatedJSONs> stream1 =
        Stream<PaginatedJSONs>.value(paginatedJSON1);

    final Stream<PaginatedJSONs> stream2 =
        Stream<PaginatedJSONs>.value(paginatedJSON2);

    final IQueryClause queryClause = IQueryClause.where(
        comparator: IWhereComparator.greaterThan(value: 20), field: "age");

    final IQueryClause queryClause2 = IQueryClause.where(
        comparator: IWhereComparator.lessThan(value: 2), field: "children");

    when(
      reactiveDatabase.readWhere(
        table: 'table',
        queryClauses: <IQueryClause>[queryClause],
      ),
    ).thenAnswer((_) => stream1);

    when(
      reactiveDatabase.readWhere(
        table: 'table',
        queryClauses: <IQueryClause>[queryClause2],
      ),
    ).thenAnswer((_) => stream2);

    // Act
    final Stream<PaginatedJSONs> result1 = cachingReactiveDatabase.readWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause],
    );
    final Stream<PaginatedJSONs> result2 = cachingReactiveDatabase.readWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause2],
    );

    //Assert
    expect(result1, isNot(result2));
  });
}
