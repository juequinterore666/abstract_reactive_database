part of caching_reactive_database_test;

void dropTable() {
  test('should pass call to received database', () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    // Act
    cachingReactiveDatabase.dropTable(
      table: 'table',
    );

    // Assert
    verify(
      reactiveDatabase.dropTable(
        table: 'table',
      ),
    );
  });
}
