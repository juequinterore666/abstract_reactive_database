part of caching_reactive_database_test;

void deleteWhere() {
  test('should pass call to received database', () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final CachingReactiveDatabase cachingReactiveDatabase =
        CachingReactiveDatabase(reactiveDatabase);

    final IQueryClause queryClause = IQueryClause.where(
        comparator: IWhereComparator.inQuery(values: <int>[25, 26, 27]),
        field: 'age');

    // Act
    cachingReactiveDatabase.deleteWhere(
      table: 'table',
      queryClauses: <IQueryClause>[queryClause],
    );

    // Assert
    verify(
      reactiveDatabase.deleteWhere(
        table: 'table',
        queryClauses: <IQueryClause>[queryClause],
      ),
    );
  });
}
