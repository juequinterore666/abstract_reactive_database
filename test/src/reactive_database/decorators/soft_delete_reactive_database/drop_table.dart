part of soft_delete_reactive_database_test;

void dropTable() {
  test('should pass call to decorated reactiveDatabase instance', () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    // Act
    await softDeleteReactiveDatabase.dropTable(table: 'table');

    // Assert
    verify(reactiveDatabase.dropTable(table: 'table')).called(1);
  });
}
