part of soft_delete_reactive_database_test;

void read() {
  test('should emit null if no register is indexed by received id', () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: anyNamed('queryClauses')))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[],
        ),
      ),
    );

    // Act
    final JSON? document =
        await softDeleteReactiveDatabase.read(table: 'table', id: 'id').first;

    // Assert
    expect(document, isNull);
  });

  test('should emit register indexed by received id', () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    final JSON expectedDocument = <String, dynamic>{'id': 'id', 'name': 'name'};

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: anyNamed('queryClauses')))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[expectedDocument],
        ),
      ),
    );

    // Act
    final JSON? document =
        await softDeleteReactiveDatabase.read(table: 'table', id: 'id').first;

    // Assert
    expect(document, expectedDocument);
  });

  test(
      "should add isDeleted: false queryClause when requesting for register by id",
      () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    final JSON expectedDocument = <String, dynamic>{'id': 'id', 'name': 'name'};

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: anyNamed('queryClauses')))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[expectedDocument],
        ),
      ),
    );

    // Act
    await softDeleteReactiveDatabase.read(table: 'table', id: 'id').first;

    // Assert
    verify(reactiveDatabase.readWhere(
      table: 'table',
      queryClauses: argThat(
          contains(
            IQueryClause.where(
              field: 'isDeleted',
              comparator: IWhereComparator.equals(value: false),
            ),
          ),
          named: 'queryClauses'),
    ));
  });
}
