part of soft_delete_reactive_database_test;

void update() {
  test('should pass call to received database adding isDeleted false to json',
      () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);
    final JSONAndId jsonAndId =
        JSONAndId(id: 'id', json: <String, dynamic>{"a": 1, "b": 2});
    final JSONAndId jsonAndIdWithIsDeleted = JSONAndId(
        id: 'id', json: <String, dynamic>{"a": 1, "b": 2, "isDeleted": false});

    // Act
    softDeleteReactiveDatabase.update(
      table: 'table',
      jsonsAndIds: <JSONAndId>[
        jsonAndId,
      ],
    );

    // Assert
    verify(
      reactiveDatabase.update(
        table: 'table',
        jsonsAndIds: <JSONAndId>[
          jsonAndIdWithIsDeleted,
        ],
      ),
    );
  });
}
