part of soft_delete_reactive_database_test;

void readWhere() {
  test('should emit empty list if no registers are indexed by received queries',
      () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: anyNamed('queryClauses')))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[],
        ),
      ),
    );

    // Act
    final PaginatedJSONs documents = await softDeleteReactiveDatabase
        .readWhere(table: 'table', queryClauses: <IQueryClause>[]).first;

    // Assert
    expect(documents, PaginatedJSONs(documents: <JSON>[], nextSkip: null));
  });

  test('should emit registers indexed by received queries', () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    final JSON register1 = <String, dynamic>{
      'id': 'id1',
      'isDeleted': false,
    };

    final JSON register2 = <String, dynamic>{
      'id': 'id2',
      'isDeleted': false,
    };

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: anyNamed('queryClauses')))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[register1, register2],
        ),
      ),
    );

    // Act
    final PaginatedJSONs documents = await softDeleteReactiveDatabase
        .readWhere(table: 'table', queryClauses: <IQueryClause>[]).first;

    // Assert
    expect(
        documents,
        PaginatedJSONs(
            documents: <JSON>[register1, register2], nextSkip: null));
  });

  test(
      "should add isDeleted: false as the last Where (if other where clauses exist) when requesting for registers by queryClauses",
      () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    final IQueryClause queryClause1 = IQueryClause.where(
        comparator: IWhereComparator.equals(value: 'Albert'), field: 'name');

    final IQueryClause queryClause2 = IQueryClause.where(
        comparator: IWhereComparator.equals(value: 'Einstein'),
        field: 'lastName');

    final IQueryClause queryClause3 = IQueryClause.where(
        comparator: IWhereComparator.greaterThan(value: 25), field: 'age');

    final IQueryClause sortClause = IQueryClause.sort(
      field: 'age',
      order: SortQueryClauseOrder.asc,
    );

    final List<IQueryClause> queryClauses = <IQueryClause>[
      queryClause1,
      queryClause2,
      queryClause3,
      sortClause,
    ];

    final JSON expectedDocument1 = <String, dynamic>{
      'id': 'id',
      'name': 'Albert',
      'lastName': 'Einstein',
      'age': 26,
    };

    final JSON expectedDocument2 = <String, dynamic>{
      'id': 'id',
      'name': 'Albert',
      'lastName': 'Einstein',
      'age': 27,
    };

    final List<IQueryClause> expectedQueryClauses = <IQueryClause>[
      queryClause1,
      queryClause2,
      queryClause3,
      IQueryClause.where(
          comparator: IWhereComparator.equals(value: false),
          field: 'isDeleted'),
      sortClause,
    ];

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: expectedQueryClauses))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[expectedDocument1, expectedDocument2],
        ),
      ),
    );

    // Act
    final PaginatedJSONs documents = await softDeleteReactiveDatabase
        .readWhere(table: 'table', queryClauses: queryClauses)
        .first;

    // Assert
    expect(
        documents,
        PaginatedJSONs(
            documents: <JSON>[expectedDocument1, expectedDocument2],
            nextSkip: null));
  });

  test(
      "should add isDeleted: false as the first query clause (if NO other where clauses exist) when requesting for registers by queryClauses",
      () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    final IQueryClause sortClause = IQueryClause.sort(
      field: 'age',
      order: SortQueryClauseOrder.asc,
    );

    final List<IQueryClause> queryClauses = <IQueryClause>[
      sortClause,
    ];

    final JSON expectedDocument1 = <String, dynamic>{
      'id': 'id',
      'name': 'Albert',
      'lastName': 'Einstein',
      'age': 26,
    };

    final JSON expectedDocument2 = <String, dynamic>{
      'id': 'id',
      'name': 'Albert',
      'lastName': 'Einstein',
      'age': 27,
    };

    final List<IQueryClause> expectedQueryClauses = <IQueryClause>[
      IQueryClause.where(
          comparator: IWhereComparator.equals(value: false),
          field: 'isDeleted'),
      sortClause,
    ];

    when(reactiveDatabase.readWhere(
            table: 'table', queryClauses: expectedQueryClauses))
        .thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        PaginatedJSONs(
          nextSkip: null,
          documents: <JSON>[expectedDocument1, expectedDocument2],
        ),
      ),
    );

    // Act
    final PaginatedJSONs documents = await softDeleteReactiveDatabase
        .readWhere(table: 'table', queryClauses: queryClauses)
        .first;

    // Assert
    expect(
        documents,
        PaginatedJSONs(
            documents: <JSON>[expectedDocument1, expectedDocument2],
            nextSkip: null));
  });
}
