part of soft_delete_reactive_database_test;

void createMultiple() {
  test(
      "should pass call to received database adding isDeleted false to all jsons",
      () {
    // Arrange
    final IReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);
    final JSONAndId jsonAndId =
        JSONAndId(id: 'id', json: <String, dynamic>{"a": 1, "b": 2});
    final JSONAndId jsonAndId2 =
        JSONAndId(id: 'id2', json: <String, dynamic>{"a": 3, "b": 4});
    final List<JSONAndId> jsonAndIds = <JSONAndId>[jsonAndId, jsonAndId2];

    final JSONAndId jsonAndIdWithIsDeleted = JSONAndId(
        id: 'id', json: <String, dynamic>{"a": 1, "b": 2, "isDeleted": false});
    final JSONAndId jsonAndId2WithIsDeleted = JSONAndId(
        id: 'id2', json: <String, dynamic>{"a": 3, "b": 4, "isDeleted": false});
    final List<JSONAndId> jsonAndIdsWithIsDeleted = <JSONAndId>[
      jsonAndIdWithIsDeleted,
      jsonAndId2WithIsDeleted
    ];

    // Act

    softDeleteReactiveDatabase.create(table: 'table', jsonsAndIds: jsonAndIds);

    //Assert
    verify(reactiveDatabase.create(
        table: 'table', jsonsAndIds: jsonAndIdsWithIsDeleted));
  });
}
