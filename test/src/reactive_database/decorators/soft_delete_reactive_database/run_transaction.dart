part of soft_delete_reactive_database_test;

void runTransaction() {
  test('should pass call to received database adding isDeleted false to json',
      () {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();

    when(
      reactiveDatabase.runTransaction(any),
    ).thenAnswer(
      (_) async {},
    );

    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);

    Future<void> transactionHandler(IReactiveDatabase transaction) async {}

    // Act
    softDeleteReactiveDatabase.runTransaction(transactionHandler);

    // Assert
    verify(
      reactiveDatabase.runTransaction(transactionHandler),
    );
  });
}
