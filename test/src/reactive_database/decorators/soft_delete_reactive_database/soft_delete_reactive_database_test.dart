library soft_delete_reactive_database_test;

import 'package:abstract_reactive_database/src/lib.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'soft_delete_reactive_database_test.mocks.dart';

part 'create.dart';
part 'create_multiple.dart';
part 'delete.dart';
part 'delete_multiple.dart';
part 'delete_where.dart';
part 'drop_table.dart';
part 'read.dart';
part 'read_where.dart';
part 'run_transaction.dart';
part 'update.dart';
part 'upgrade.dart';

@GenerateMocks(<Type>[IReactiveDatabase])
void main() {
  group('SoftDeleteReactiveDatabase', () {
    group('create', () {
      create();
    });

    group('createMultiple', () {
      createMultiple();
    });

    group('delete', () {
      delete();
    });

    group('deleteWhere', () {
      deleteWhere();
    });

    group('deleteMultiple', () {
      deleteMultiple();
    });

    group('dropTable', () {
      dropTable();
    });

    group('read', () {
      read();
    });

    group('readWhere', () {
      readWhere();
    });

    group('runTransaction', () {
      runTransaction();
    });

    group('update', () {
      update();
    });

    group('upsert', () {
      upsert();
    });
  });
}
