part of soft_delete_reactive_database_test;

void deleteWhere() {
  test(
      'should call upsertAll with documents that match received queryClauses and updating their data to isDeleted: true when the documents have id field',
      () async {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);
    final List<IQueryClause> queryClauses = <IQueryClause>[
      IQueryClause.where(
        comparator: IWhereComparator.greaterThan(value: 25),
        field: 'age',
      )
    ];
    final JSON json = <String, dynamic>{
      "age": 30,
      "name": "John",
      "id": "id",
    };
    final JSON json2 = <String, dynamic>{
      "age": 32,
      "name": "James",
      "id": "id2",
    };

    final PaginatedJSONs paginatedJSONs =
        PaginatedJSONs(documents: <JSON>[json, json2], nextSkip: 'nextSkip');

    when(
      reactiveDatabase.readWhere(
        table: 'table',
        queryClauses: argThat(
          containsAll(
            <IQueryClause>[
              ...queryClauses,
              IQueryClause.where(
                  comparator: IWhereComparator.equals(value: false),
                  field: 'isDeleted')
            ],
          ),
          named: "queryClauses",
        ),
      ),
    ).thenAnswer(
      (_) => Stream<PaginatedJSONs>.value(
        paginatedJSONs,
      ),
    );

    // Act
    await softDeleteReactiveDatabase.deleteWhere(
        table: 'table', queryClauses: queryClauses);

    //Assert
    verify(
      reactiveDatabase.upsert(
        table: 'table',
        jsonsAndIds: argThat(
          containsAllInOrder(
            <JSONAndId>[
              JSONAndId(id: 'id', json: <String, dynamic>{"isDeleted": true}),
              JSONAndId(
                id: 'id2',
                json: <String, dynamic>{"isDeleted": true},
              )
            ],
          ),
          named: "jsonsAndIds",
        ),
      ),
    );
  });
}
