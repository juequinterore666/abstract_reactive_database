part of soft_delete_reactive_database_test;

void delete() {
  test("should send a JsonWithId containing received id with isDeleted: true",
      () {
    // Arrange
    final MockIReactiveDatabase reactiveDatabase = MockIReactiveDatabase();
    final SoftDeleteReactiveDatabase softDeleteReactiveDatabase =
        SoftDeleteReactiveDatabase(reactiveDatabase);
    final JSONAndId jsonAndIdWithIsDeleted =
        JSONAndId(id: 'id', json: <String, dynamic>{"isDeleted": true});

    // Act
    softDeleteReactiveDatabase.delete(table: 'table', ids: <String>['id']);

    //Assert
    verify(reactiveDatabase.upsert(
        table: 'table', jsonsAndIds: <JSONAndId>[jsonAndIdWithIsDeleted]));
  });
}
