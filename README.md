Base logic to abstract any reactive database engine.
In this way it's possible to adapt any reactive database engine to the same interface without the need to change your app code.

It includes different decorators including:
- caching_reactive_database: It prevents to re-query the database if the same query is already running.
- soft_delete_reactive_database: It prevents to delete the record from the database, instead it sets a flag to mark it as deleted. Reads take that flag into account.
- write_date_tracker_reactive_database: It adds creation and update date fields to the records.

This is just the abstraction layer, you need to install the specific reactive database engine you want to use and its adapter.