import 'package:freezed_annotation/freezed_annotation.dart';

part 'i_where_comparator.freezed.dart';
part 'i_where_comparator.g.dart';

@freezed

/// Abstraction over where clauses
class IWhereComparator with _$IWhereComparator {
  /// Query clause for querying items where a given array field includes [value].
  const factory IWhereComparator.arrayIncludes({required Object value}) =
      _ArrayIncludes;

  /// Query clause for querying items where a given array field includes any of the values inside [value].
  const factory IWhereComparator.arrayIncludesAny(
      {required Iterable<Object> value}) = _ArrayIncludesAnyWhereComparator;

  /// Query clause for querying items where a given field equals [value].
  const factory IWhereComparator.equals({required Object value}) =
      _EqualsWhereComparator;

  /// Query clause for querying items where a given field is greater than [value].
  const factory IWhereComparator.greaterThan({required Object value}) =
      _GreaterThanWhereComparator;

  /// Query clause for querying items where a given field is greater than or equals [value].
  const factory IWhereComparator.greaterThanOrEquals({required Object value}) =
      _GreaterThanOrEqualsWhereComparator;

  /// Query clause for querying items where a given field value is contained in [values].
  const factory IWhereComparator.inQuery({required List<Object> values}) =
      _InWhereComparator;

  /// Query clause for querying items where a given field value is not contained in [values].
  const factory IWhereComparator.notInQuery({required List<Object> values}) =
      _NotInWhereComparator;

  /// Query clause for querying items where a given field is less than [value].
  const factory IWhereComparator.lessThan({required Object value}) =
      _LessThanWhereComparator;

  /// Query clause for querying items where a given field is less than or equals [value].
  const factory IWhereComparator.lessThanOrEquals({required Object value}) =
      _LessThanOrEqualsWhereComparator;

  /// Query clause for querying items where a given field is not equals [value].
  const factory IWhereComparator.notEquals({required Object value}) =
      _NotEqualsWhereComparator;

  /// Create IWhereComparator from json
  // coverage:ignore-start
  factory IWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$IWhereComparatorFromJson(json);
  // coverage:ignore-end
}
