// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'i_where_comparator.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IWhereComparator _$IWhereComparatorFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'arrayIncludes':
      return _ArrayIncludes.fromJson(json);
    case 'arrayIncludesAny':
      return _ArrayIncludesAnyWhereComparator.fromJson(json);
    case 'equals':
      return _EqualsWhereComparator.fromJson(json);
    case 'greaterThan':
      return _GreaterThanWhereComparator.fromJson(json);
    case 'greaterThanOrEquals':
      return _GreaterThanOrEqualsWhereComparator.fromJson(json);
    case 'inQuery':
      return _InWhereComparator.fromJson(json);
    case 'notInQuery':
      return _NotInWhereComparator.fromJson(json);
    case 'lessThan':
      return _LessThanWhereComparator.fromJson(json);
    case 'lessThanOrEquals':
      return _LessThanOrEqualsWhereComparator.fromJson(json);
    case 'notEquals':
      return _NotEqualsWhereComparator.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'IWhereComparator',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
mixin _$IWhereComparator {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IWhereComparatorCopyWith<$Res> {
  factory $IWhereComparatorCopyWith(
          IWhereComparator value, $Res Function(IWhereComparator) then) =
      _$IWhereComparatorCopyWithImpl<$Res, IWhereComparator>;
}

/// @nodoc
class _$IWhereComparatorCopyWithImpl<$Res, $Val extends IWhereComparator>
    implements $IWhereComparatorCopyWith<$Res> {
  _$IWhereComparatorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_ArrayIncludesCopyWith<$Res> {
  factory _$$_ArrayIncludesCopyWith(
          _$_ArrayIncludes value, $Res Function(_$_ArrayIncludes) then) =
      __$$_ArrayIncludesCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_ArrayIncludesCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_ArrayIncludes>
    implements _$$_ArrayIncludesCopyWith<$Res> {
  __$$_ArrayIncludesCopyWithImpl(
      _$_ArrayIncludes _value, $Res Function(_$_ArrayIncludes) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_ArrayIncludes(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ArrayIncludes implements _ArrayIncludes {
  const _$_ArrayIncludes({required this.value, final String? $type})
      : $type = $type ?? 'arrayIncludes';

  factory _$_ArrayIncludes.fromJson(Map<String, dynamic> json) =>
      _$$_ArrayIncludesFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.arrayIncludes(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ArrayIncludes &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ArrayIncludesCopyWith<_$_ArrayIncludes> get copyWith =>
      __$$_ArrayIncludesCopyWithImpl<_$_ArrayIncludes>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return arrayIncludes(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return arrayIncludes?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (arrayIncludes != null) {
      return arrayIncludes(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return arrayIncludes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return arrayIncludes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (arrayIncludes != null) {
      return arrayIncludes(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_ArrayIncludesToJson(
      this,
    );
  }
}

abstract class _ArrayIncludes implements IWhereComparator {
  const factory _ArrayIncludes({required final Object value}) =
      _$_ArrayIncludes;

  factory _ArrayIncludes.fromJson(Map<String, dynamic> json) =
      _$_ArrayIncludes.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_ArrayIncludesCopyWith<_$_ArrayIncludes> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ArrayIncludesAnyWhereComparatorCopyWith<$Res> {
  factory _$$_ArrayIncludesAnyWhereComparatorCopyWith(
          _$_ArrayIncludesAnyWhereComparator value,
          $Res Function(_$_ArrayIncludesAnyWhereComparator) then) =
      __$$_ArrayIncludesAnyWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Iterable<Object> value});
}

/// @nodoc
class __$$_ArrayIncludesAnyWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res,
        _$_ArrayIncludesAnyWhereComparator>
    implements _$$_ArrayIncludesAnyWhereComparatorCopyWith<$Res> {
  __$$_ArrayIncludesAnyWhereComparatorCopyWithImpl(
      _$_ArrayIncludesAnyWhereComparator _value,
      $Res Function(_$_ArrayIncludesAnyWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_ArrayIncludesAnyWhereComparator(
      value: null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Iterable<Object>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ArrayIncludesAnyWhereComparator
    implements _ArrayIncludesAnyWhereComparator {
  const _$_ArrayIncludesAnyWhereComparator(
      {required this.value, final String? $type})
      : $type = $type ?? 'arrayIncludesAny';

  factory _$_ArrayIncludesAnyWhereComparator.fromJson(
          Map<String, dynamic> json) =>
      _$$_ArrayIncludesAnyWhereComparatorFromJson(json);

  @override
  final Iterable<Object> value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.arrayIncludesAny(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ArrayIncludesAnyWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ArrayIncludesAnyWhereComparatorCopyWith<
          _$_ArrayIncludesAnyWhereComparator>
      get copyWith => __$$_ArrayIncludesAnyWhereComparatorCopyWithImpl<
          _$_ArrayIncludesAnyWhereComparator>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return arrayIncludesAny(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return arrayIncludesAny?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (arrayIncludesAny != null) {
      return arrayIncludesAny(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return arrayIncludesAny(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return arrayIncludesAny?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (arrayIncludesAny != null) {
      return arrayIncludesAny(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_ArrayIncludesAnyWhereComparatorToJson(
      this,
    );
  }
}

abstract class _ArrayIncludesAnyWhereComparator implements IWhereComparator {
  const factory _ArrayIncludesAnyWhereComparator(
          {required final Iterable<Object> value}) =
      _$_ArrayIncludesAnyWhereComparator;

  factory _ArrayIncludesAnyWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_ArrayIncludesAnyWhereComparator.fromJson;

  Iterable<Object> get value;
  @JsonKey(ignore: true)
  _$$_ArrayIncludesAnyWhereComparatorCopyWith<
          _$_ArrayIncludesAnyWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EqualsWhereComparatorCopyWith<$Res> {
  factory _$$_EqualsWhereComparatorCopyWith(_$_EqualsWhereComparator value,
          $Res Function(_$_EqualsWhereComparator) then) =
      __$$_EqualsWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_EqualsWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_EqualsWhereComparator>
    implements _$$_EqualsWhereComparatorCopyWith<$Res> {
  __$$_EqualsWhereComparatorCopyWithImpl(_$_EqualsWhereComparator _value,
      $Res Function(_$_EqualsWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_EqualsWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_EqualsWhereComparator implements _EqualsWhereComparator {
  const _$_EqualsWhereComparator({required this.value, final String? $type})
      : $type = $type ?? 'equals';

  factory _$_EqualsWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_EqualsWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.equals(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EqualsWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EqualsWhereComparatorCopyWith<_$_EqualsWhereComparator> get copyWith =>
      __$$_EqualsWhereComparatorCopyWithImpl<_$_EqualsWhereComparator>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return equals(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return equals?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (equals != null) {
      return equals(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return equals(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return equals?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (equals != null) {
      return equals(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_EqualsWhereComparatorToJson(
      this,
    );
  }
}

abstract class _EqualsWhereComparator implements IWhereComparator {
  const factory _EqualsWhereComparator({required final Object value}) =
      _$_EqualsWhereComparator;

  factory _EqualsWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_EqualsWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_EqualsWhereComparatorCopyWith<_$_EqualsWhereComparator> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GreaterThanWhereComparatorCopyWith<$Res> {
  factory _$$_GreaterThanWhereComparatorCopyWith(
          _$_GreaterThanWhereComparator value,
          $Res Function(_$_GreaterThanWhereComparator) then) =
      __$$_GreaterThanWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_GreaterThanWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_GreaterThanWhereComparator>
    implements _$$_GreaterThanWhereComparatorCopyWith<$Res> {
  __$$_GreaterThanWhereComparatorCopyWithImpl(
      _$_GreaterThanWhereComparator _value,
      $Res Function(_$_GreaterThanWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_GreaterThanWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GreaterThanWhereComparator implements _GreaterThanWhereComparator {
  const _$_GreaterThanWhereComparator(
      {required this.value, final String? $type})
      : $type = $type ?? 'greaterThan';

  factory _$_GreaterThanWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_GreaterThanWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.greaterThan(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GreaterThanWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GreaterThanWhereComparatorCopyWith<_$_GreaterThanWhereComparator>
      get copyWith => __$$_GreaterThanWhereComparatorCopyWithImpl<
          _$_GreaterThanWhereComparator>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return greaterThan(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return greaterThan?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (greaterThan != null) {
      return greaterThan(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return greaterThan(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return greaterThan?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (greaterThan != null) {
      return greaterThan(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_GreaterThanWhereComparatorToJson(
      this,
    );
  }
}

abstract class _GreaterThanWhereComparator implements IWhereComparator {
  const factory _GreaterThanWhereComparator({required final Object value}) =
      _$_GreaterThanWhereComparator;

  factory _GreaterThanWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_GreaterThanWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_GreaterThanWhereComparatorCopyWith<_$_GreaterThanWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GreaterThanOrEqualsWhereComparatorCopyWith<$Res> {
  factory _$$_GreaterThanOrEqualsWhereComparatorCopyWith(
          _$_GreaterThanOrEqualsWhereComparator value,
          $Res Function(_$_GreaterThanOrEqualsWhereComparator) then) =
      __$$_GreaterThanOrEqualsWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_GreaterThanOrEqualsWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res,
        _$_GreaterThanOrEqualsWhereComparator>
    implements _$$_GreaterThanOrEqualsWhereComparatorCopyWith<$Res> {
  __$$_GreaterThanOrEqualsWhereComparatorCopyWithImpl(
      _$_GreaterThanOrEqualsWhereComparator _value,
      $Res Function(_$_GreaterThanOrEqualsWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_GreaterThanOrEqualsWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_GreaterThanOrEqualsWhereComparator
    implements _GreaterThanOrEqualsWhereComparator {
  const _$_GreaterThanOrEqualsWhereComparator(
      {required this.value, final String? $type})
      : $type = $type ?? 'greaterThanOrEquals';

  factory _$_GreaterThanOrEqualsWhereComparator.fromJson(
          Map<String, dynamic> json) =>
      _$$_GreaterThanOrEqualsWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.greaterThanOrEquals(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GreaterThanOrEqualsWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_GreaterThanOrEqualsWhereComparatorCopyWith<
          _$_GreaterThanOrEqualsWhereComparator>
      get copyWith => __$$_GreaterThanOrEqualsWhereComparatorCopyWithImpl<
          _$_GreaterThanOrEqualsWhereComparator>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return greaterThanOrEquals(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return greaterThanOrEquals?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (greaterThanOrEquals != null) {
      return greaterThanOrEquals(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return greaterThanOrEquals(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return greaterThanOrEquals?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (greaterThanOrEquals != null) {
      return greaterThanOrEquals(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_GreaterThanOrEqualsWhereComparatorToJson(
      this,
    );
  }
}

abstract class _GreaterThanOrEqualsWhereComparator implements IWhereComparator {
  const factory _GreaterThanOrEqualsWhereComparator(
      {required final Object value}) = _$_GreaterThanOrEqualsWhereComparator;

  factory _GreaterThanOrEqualsWhereComparator.fromJson(
          Map<String, dynamic> json) =
      _$_GreaterThanOrEqualsWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_GreaterThanOrEqualsWhereComparatorCopyWith<
          _$_GreaterThanOrEqualsWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_InWhereComparatorCopyWith<$Res> {
  factory _$$_InWhereComparatorCopyWith(_$_InWhereComparator value,
          $Res Function(_$_InWhereComparator) then) =
      __$$_InWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Object> values});
}

/// @nodoc
class __$$_InWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_InWhereComparator>
    implements _$$_InWhereComparatorCopyWith<$Res> {
  __$$_InWhereComparatorCopyWithImpl(
      _$_InWhereComparator _value, $Res Function(_$_InWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? values = null,
  }) {
    return _then(_$_InWhereComparator(
      values: null == values
          ? _value._values
          : values // ignore: cast_nullable_to_non_nullable
              as List<Object>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_InWhereComparator implements _InWhereComparator {
  const _$_InWhereComparator(
      {required final List<Object> values, final String? $type})
      : _values = values,
        $type = $type ?? 'inQuery';

  factory _$_InWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_InWhereComparatorFromJson(json);

  final List<Object> _values;
  @override
  List<Object> get values {
    if (_values is EqualUnmodifiableListView) return _values;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_values);
  }

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.inQuery(values: $values)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_InWhereComparator &&
            const DeepCollectionEquality().equals(other._values, _values));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_values));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_InWhereComparatorCopyWith<_$_InWhereComparator> get copyWith =>
      __$$_InWhereComparatorCopyWithImpl<_$_InWhereComparator>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return inQuery(values);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return inQuery?.call(values);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (inQuery != null) {
      return inQuery(values);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return inQuery(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return inQuery?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (inQuery != null) {
      return inQuery(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_InWhereComparatorToJson(
      this,
    );
  }
}

abstract class _InWhereComparator implements IWhereComparator {
  const factory _InWhereComparator({required final List<Object> values}) =
      _$_InWhereComparator;

  factory _InWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_InWhereComparator.fromJson;

  List<Object> get values;
  @JsonKey(ignore: true)
  _$$_InWhereComparatorCopyWith<_$_InWhereComparator> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_NotInWhereComparatorCopyWith<$Res> {
  factory _$$_NotInWhereComparatorCopyWith(_$_NotInWhereComparator value,
          $Res Function(_$_NotInWhereComparator) then) =
      __$$_NotInWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({List<Object> values});
}

/// @nodoc
class __$$_NotInWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_NotInWhereComparator>
    implements _$$_NotInWhereComparatorCopyWith<$Res> {
  __$$_NotInWhereComparatorCopyWithImpl(_$_NotInWhereComparator _value,
      $Res Function(_$_NotInWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? values = null,
  }) {
    return _then(_$_NotInWhereComparator(
      values: null == values
          ? _value._values
          : values // ignore: cast_nullable_to_non_nullable
              as List<Object>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NotInWhereComparator implements _NotInWhereComparator {
  const _$_NotInWhereComparator(
      {required final List<Object> values, final String? $type})
      : _values = values,
        $type = $type ?? 'notInQuery';

  factory _$_NotInWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_NotInWhereComparatorFromJson(json);

  final List<Object> _values;
  @override
  List<Object> get values {
    if (_values is EqualUnmodifiableListView) return _values;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_values);
  }

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.notInQuery(values: $values)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_NotInWhereComparator &&
            const DeepCollectionEquality().equals(other._values, _values));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_values));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_NotInWhereComparatorCopyWith<_$_NotInWhereComparator> get copyWith =>
      __$$_NotInWhereComparatorCopyWithImpl<_$_NotInWhereComparator>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return notInQuery(values);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return notInQuery?.call(values);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (notInQuery != null) {
      return notInQuery(values);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return notInQuery(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return notInQuery?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (notInQuery != null) {
      return notInQuery(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_NotInWhereComparatorToJson(
      this,
    );
  }
}

abstract class _NotInWhereComparator implements IWhereComparator {
  const factory _NotInWhereComparator({required final List<Object> values}) =
      _$_NotInWhereComparator;

  factory _NotInWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_NotInWhereComparator.fromJson;

  List<Object> get values;
  @JsonKey(ignore: true)
  _$$_NotInWhereComparatorCopyWith<_$_NotInWhereComparator> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_LessThanWhereComparatorCopyWith<$Res> {
  factory _$$_LessThanWhereComparatorCopyWith(_$_LessThanWhereComparator value,
          $Res Function(_$_LessThanWhereComparator) then) =
      __$$_LessThanWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_LessThanWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_LessThanWhereComparator>
    implements _$$_LessThanWhereComparatorCopyWith<$Res> {
  __$$_LessThanWhereComparatorCopyWithImpl(_$_LessThanWhereComparator _value,
      $Res Function(_$_LessThanWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_LessThanWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LessThanWhereComparator implements _LessThanWhereComparator {
  const _$_LessThanWhereComparator({required this.value, final String? $type})
      : $type = $type ?? 'lessThan';

  factory _$_LessThanWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_LessThanWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.lessThan(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LessThanWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LessThanWhereComparatorCopyWith<_$_LessThanWhereComparator>
      get copyWith =>
          __$$_LessThanWhereComparatorCopyWithImpl<_$_LessThanWhereComparator>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return lessThan(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return lessThan?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (lessThan != null) {
      return lessThan(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return lessThan(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return lessThan?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (lessThan != null) {
      return lessThan(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_LessThanWhereComparatorToJson(
      this,
    );
  }
}

abstract class _LessThanWhereComparator implements IWhereComparator {
  const factory _LessThanWhereComparator({required final Object value}) =
      _$_LessThanWhereComparator;

  factory _LessThanWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_LessThanWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_LessThanWhereComparatorCopyWith<_$_LessThanWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_LessThanOrEqualsWhereComparatorCopyWith<$Res> {
  factory _$$_LessThanOrEqualsWhereComparatorCopyWith(
          _$_LessThanOrEqualsWhereComparator value,
          $Res Function(_$_LessThanOrEqualsWhereComparator) then) =
      __$$_LessThanOrEqualsWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_LessThanOrEqualsWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res,
        _$_LessThanOrEqualsWhereComparator>
    implements _$$_LessThanOrEqualsWhereComparatorCopyWith<$Res> {
  __$$_LessThanOrEqualsWhereComparatorCopyWithImpl(
      _$_LessThanOrEqualsWhereComparator _value,
      $Res Function(_$_LessThanOrEqualsWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_LessThanOrEqualsWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LessThanOrEqualsWhereComparator
    implements _LessThanOrEqualsWhereComparator {
  const _$_LessThanOrEqualsWhereComparator(
      {required this.value, final String? $type})
      : $type = $type ?? 'lessThanOrEquals';

  factory _$_LessThanOrEqualsWhereComparator.fromJson(
          Map<String, dynamic> json) =>
      _$$_LessThanOrEqualsWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.lessThanOrEquals(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LessThanOrEqualsWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LessThanOrEqualsWhereComparatorCopyWith<
          _$_LessThanOrEqualsWhereComparator>
      get copyWith => __$$_LessThanOrEqualsWhereComparatorCopyWithImpl<
          _$_LessThanOrEqualsWhereComparator>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return lessThanOrEquals(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return lessThanOrEquals?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (lessThanOrEquals != null) {
      return lessThanOrEquals(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return lessThanOrEquals(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return lessThanOrEquals?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (lessThanOrEquals != null) {
      return lessThanOrEquals(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_LessThanOrEqualsWhereComparatorToJson(
      this,
    );
  }
}

abstract class _LessThanOrEqualsWhereComparator implements IWhereComparator {
  const factory _LessThanOrEqualsWhereComparator(
      {required final Object value}) = _$_LessThanOrEqualsWhereComparator;

  factory _LessThanOrEqualsWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_LessThanOrEqualsWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_LessThanOrEqualsWhereComparatorCopyWith<
          _$_LessThanOrEqualsWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_NotEqualsWhereComparatorCopyWith<$Res> {
  factory _$$_NotEqualsWhereComparatorCopyWith(
          _$_NotEqualsWhereComparator value,
          $Res Function(_$_NotEqualsWhereComparator) then) =
      __$$_NotEqualsWhereComparatorCopyWithImpl<$Res>;
  @useResult
  $Res call({Object value});
}

/// @nodoc
class __$$_NotEqualsWhereComparatorCopyWithImpl<$Res>
    extends _$IWhereComparatorCopyWithImpl<$Res, _$_NotEqualsWhereComparator>
    implements _$$_NotEqualsWhereComparatorCopyWith<$Res> {
  __$$_NotEqualsWhereComparatorCopyWithImpl(_$_NotEqualsWhereComparator _value,
      $Res Function(_$_NotEqualsWhereComparator) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$_NotEqualsWhereComparator(
      value: null == value ? _value.value : value,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_NotEqualsWhereComparator implements _NotEqualsWhereComparator {
  const _$_NotEqualsWhereComparator({required this.value, final String? $type})
      : $type = $type ?? 'notEquals';

  factory _$_NotEqualsWhereComparator.fromJson(Map<String, dynamic> json) =>
      _$$_NotEqualsWhereComparatorFromJson(json);

  @override
  final Object value;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IWhereComparator.notEquals(value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_NotEqualsWhereComparator &&
            const DeepCollectionEquality().equals(other.value, value));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(value));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_NotEqualsWhereComparatorCopyWith<_$_NotEqualsWhereComparator>
      get copyWith => __$$_NotEqualsWhereComparatorCopyWithImpl<
          _$_NotEqualsWhereComparator>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Object value) arrayIncludes,
    required TResult Function(Iterable<Object> value) arrayIncludesAny,
    required TResult Function(Object value) equals,
    required TResult Function(Object value) greaterThan,
    required TResult Function(Object value) greaterThanOrEquals,
    required TResult Function(List<Object> values) inQuery,
    required TResult Function(List<Object> values) notInQuery,
    required TResult Function(Object value) lessThan,
    required TResult Function(Object value) lessThanOrEquals,
    required TResult Function(Object value) notEquals,
  }) {
    return notEquals(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(Object value)? arrayIncludes,
    TResult? Function(Iterable<Object> value)? arrayIncludesAny,
    TResult? Function(Object value)? equals,
    TResult? Function(Object value)? greaterThan,
    TResult? Function(Object value)? greaterThanOrEquals,
    TResult? Function(List<Object> values)? inQuery,
    TResult? Function(List<Object> values)? notInQuery,
    TResult? Function(Object value)? lessThan,
    TResult? Function(Object value)? lessThanOrEquals,
    TResult? Function(Object value)? notEquals,
  }) {
    return notEquals?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Object value)? arrayIncludes,
    TResult Function(Iterable<Object> value)? arrayIncludesAny,
    TResult Function(Object value)? equals,
    TResult Function(Object value)? greaterThan,
    TResult Function(Object value)? greaterThanOrEquals,
    TResult Function(List<Object> values)? inQuery,
    TResult Function(List<Object> values)? notInQuery,
    TResult Function(Object value)? lessThan,
    TResult Function(Object value)? lessThanOrEquals,
    TResult Function(Object value)? notEquals,
    required TResult orElse(),
  }) {
    if (notEquals != null) {
      return notEquals(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ArrayIncludes value) arrayIncludes,
    required TResult Function(_ArrayIncludesAnyWhereComparator value)
        arrayIncludesAny,
    required TResult Function(_EqualsWhereComparator value) equals,
    required TResult Function(_GreaterThanWhereComparator value) greaterThan,
    required TResult Function(_GreaterThanOrEqualsWhereComparator value)
        greaterThanOrEquals,
    required TResult Function(_InWhereComparator value) inQuery,
    required TResult Function(_NotInWhereComparator value) notInQuery,
    required TResult Function(_LessThanWhereComparator value) lessThan,
    required TResult Function(_LessThanOrEqualsWhereComparator value)
        lessThanOrEquals,
    required TResult Function(_NotEqualsWhereComparator value) notEquals,
  }) {
    return notEquals(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ArrayIncludes value)? arrayIncludes,
    TResult? Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult? Function(_EqualsWhereComparator value)? equals,
    TResult? Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult? Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult? Function(_InWhereComparator value)? inQuery,
    TResult? Function(_NotInWhereComparator value)? notInQuery,
    TResult? Function(_LessThanWhereComparator value)? lessThan,
    TResult? Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult? Function(_NotEqualsWhereComparator value)? notEquals,
  }) {
    return notEquals?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ArrayIncludes value)? arrayIncludes,
    TResult Function(_ArrayIncludesAnyWhereComparator value)? arrayIncludesAny,
    TResult Function(_EqualsWhereComparator value)? equals,
    TResult Function(_GreaterThanWhereComparator value)? greaterThan,
    TResult Function(_GreaterThanOrEqualsWhereComparator value)?
        greaterThanOrEquals,
    TResult Function(_InWhereComparator value)? inQuery,
    TResult Function(_NotInWhereComparator value)? notInQuery,
    TResult Function(_LessThanWhereComparator value)? lessThan,
    TResult Function(_LessThanOrEqualsWhereComparator value)? lessThanOrEquals,
    TResult Function(_NotEqualsWhereComparator value)? notEquals,
    required TResult orElse(),
  }) {
    if (notEquals != null) {
      return notEquals(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_NotEqualsWhereComparatorToJson(
      this,
    );
  }
}

abstract class _NotEqualsWhereComparator implements IWhereComparator {
  const factory _NotEqualsWhereComparator({required final Object value}) =
      _$_NotEqualsWhereComparator;

  factory _NotEqualsWhereComparator.fromJson(Map<String, dynamic> json) =
      _$_NotEqualsWhereComparator.fromJson;

  Object get value;
  @JsonKey(ignore: true)
  _$$_NotEqualsWhereComparatorCopyWith<_$_NotEqualsWhereComparator>
      get copyWith => throw _privateConstructorUsedError;
}
