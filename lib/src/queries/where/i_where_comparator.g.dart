// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'i_where_comparator.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ArrayIncludes _$$_ArrayIncludesFromJson(Map<String, dynamic> json) =>
    _$_ArrayIncludes(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_ArrayIncludesToJson(_$_ArrayIncludes instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_ArrayIncludesAnyWhereComparator _$$_ArrayIncludesAnyWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_ArrayIncludesAnyWhereComparator(
      value: (json['value'] as List<dynamic>).map((e) => e as Object),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_ArrayIncludesAnyWhereComparatorToJson(
        _$_ArrayIncludesAnyWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value.toList(),
      'runtimeType': instance.$type,
    };

_$_EqualsWhereComparator _$$_EqualsWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_EqualsWhereComparator(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_EqualsWhereComparatorToJson(
        _$_EqualsWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_GreaterThanWhereComparator _$$_GreaterThanWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_GreaterThanWhereComparator(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_GreaterThanWhereComparatorToJson(
        _$_GreaterThanWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_GreaterThanOrEqualsWhereComparator
    _$$_GreaterThanOrEqualsWhereComparatorFromJson(Map<String, dynamic> json) =>
        _$_GreaterThanOrEqualsWhereComparator(
          value: json['value'] as Object,
          $type: json['runtimeType'] as String?,
        );

Map<String, dynamic> _$$_GreaterThanOrEqualsWhereComparatorToJson(
        _$_GreaterThanOrEqualsWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_InWhereComparator _$$_InWhereComparatorFromJson(Map<String, dynamic> json) =>
    _$_InWhereComparator(
      values:
          (json['values'] as List<dynamic>).map((e) => e as Object).toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_InWhereComparatorToJson(
        _$_InWhereComparator instance) =>
    <String, dynamic>{
      'values': instance.values,
      'runtimeType': instance.$type,
    };

_$_NotInWhereComparator _$$_NotInWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_NotInWhereComparator(
      values:
          (json['values'] as List<dynamic>).map((e) => e as Object).toList(),
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_NotInWhereComparatorToJson(
        _$_NotInWhereComparator instance) =>
    <String, dynamic>{
      'values': instance.values,
      'runtimeType': instance.$type,
    };

_$_LessThanWhereComparator _$$_LessThanWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_LessThanWhereComparator(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_LessThanWhereComparatorToJson(
        _$_LessThanWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_LessThanOrEqualsWhereComparator _$$_LessThanOrEqualsWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_LessThanOrEqualsWhereComparator(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_LessThanOrEqualsWhereComparatorToJson(
        _$_LessThanOrEqualsWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };

_$_NotEqualsWhereComparator _$$_NotEqualsWhereComparatorFromJson(
        Map<String, dynamic> json) =>
    _$_NotEqualsWhereComparator(
      value: json['value'] as Object,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_NotEqualsWhereComparatorToJson(
        _$_NotEqualsWhereComparator instance) =>
    <String, dynamic>{
      'value': instance.value,
      'runtimeType': instance.$type,
    };
