// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'i_query_clause.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IQueryClause _$IQueryClauseFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType']) {
    case 'limit':
      return _Limit.fromJson(json);
    case 'skip':
      return Skip.fromJson(json);
    case 'sort':
      return _Sort.fromJson(json);
    case 'where':
      return Where.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'IQueryClause',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
mixin _$IQueryClause {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int amount) limit,
    required TResult Function(dynamic exclusivePlaceToStart) skip,
    required TResult Function(SortQueryClauseOrder order, String field) sort,
    required TResult Function(IWhereComparator comparator, String field) where,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int amount)? limit,
    TResult? Function(dynamic exclusivePlaceToStart)? skip,
    TResult? Function(SortQueryClauseOrder order, String field)? sort,
    TResult? Function(IWhereComparator comparator, String field)? where,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int amount)? limit,
    TResult Function(dynamic exclusivePlaceToStart)? skip,
    TResult Function(SortQueryClauseOrder order, String field)? sort,
    TResult Function(IWhereComparator comparator, String field)? where,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Limit value) limit,
    required TResult Function(Skip value) skip,
    required TResult Function(_Sort value) sort,
    required TResult Function(Where value) where,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Limit value)? limit,
    TResult? Function(Skip value)? skip,
    TResult? Function(_Sort value)? sort,
    TResult? Function(Where value)? where,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Limit value)? limit,
    TResult Function(Skip value)? skip,
    TResult Function(_Sort value)? sort,
    TResult Function(Where value)? where,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IQueryClauseCopyWith<$Res> {
  factory $IQueryClauseCopyWith(
          IQueryClause value, $Res Function(IQueryClause) then) =
      _$IQueryClauseCopyWithImpl<$Res, IQueryClause>;
}

/// @nodoc
class _$IQueryClauseCopyWithImpl<$Res, $Val extends IQueryClause>
    implements $IQueryClauseCopyWith<$Res> {
  _$IQueryClauseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_LimitCopyWith<$Res> {
  factory _$$_LimitCopyWith(_$_Limit value, $Res Function(_$_Limit) then) =
      __$$_LimitCopyWithImpl<$Res>;
  @useResult
  $Res call({int amount});
}

/// @nodoc
class __$$_LimitCopyWithImpl<$Res>
    extends _$IQueryClauseCopyWithImpl<$Res, _$_Limit>
    implements _$$_LimitCopyWith<$Res> {
  __$$_LimitCopyWithImpl(_$_Limit _value, $Res Function(_$_Limit) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
  }) {
    return _then(_$_Limit(
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Limit implements _Limit {
  const _$_Limit({required this.amount, final String? $type})
      : $type = $type ?? 'limit';

  factory _$_Limit.fromJson(Map<String, dynamic> json) =>
      _$$_LimitFromJson(json);

  @override
  final int amount;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IQueryClause.limit(amount: $amount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Limit &&
            (identical(other.amount, amount) || other.amount == amount));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, amount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LimitCopyWith<_$_Limit> get copyWith =>
      __$$_LimitCopyWithImpl<_$_Limit>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int amount) limit,
    required TResult Function(dynamic exclusivePlaceToStart) skip,
    required TResult Function(SortQueryClauseOrder order, String field) sort,
    required TResult Function(IWhereComparator comparator, String field) where,
  }) {
    return limit(amount);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int amount)? limit,
    TResult? Function(dynamic exclusivePlaceToStart)? skip,
    TResult? Function(SortQueryClauseOrder order, String field)? sort,
    TResult? Function(IWhereComparator comparator, String field)? where,
  }) {
    return limit?.call(amount);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int amount)? limit,
    TResult Function(dynamic exclusivePlaceToStart)? skip,
    TResult Function(SortQueryClauseOrder order, String field)? sort,
    TResult Function(IWhereComparator comparator, String field)? where,
    required TResult orElse(),
  }) {
    if (limit != null) {
      return limit(amount);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Limit value) limit,
    required TResult Function(Skip value) skip,
    required TResult Function(_Sort value) sort,
    required TResult Function(Where value) where,
  }) {
    return limit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Limit value)? limit,
    TResult? Function(Skip value)? skip,
    TResult? Function(_Sort value)? sort,
    TResult? Function(Where value)? where,
  }) {
    return limit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Limit value)? limit,
    TResult Function(Skip value)? skip,
    TResult Function(_Sort value)? sort,
    TResult Function(Where value)? where,
    required TResult orElse(),
  }) {
    if (limit != null) {
      return limit(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_LimitToJson(
      this,
    );
  }
}

abstract class _Limit implements IQueryClause {
  const factory _Limit({required final int amount}) = _$_Limit;

  factory _Limit.fromJson(Map<String, dynamic> json) = _$_Limit.fromJson;

  int get amount;
  @JsonKey(ignore: true)
  _$$_LimitCopyWith<_$_Limit> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SkipCopyWith<$Res> {
  factory _$$SkipCopyWith(_$Skip value, $Res Function(_$Skip) then) =
      __$$SkipCopyWithImpl<$Res>;
  @useResult
  $Res call({dynamic exclusivePlaceToStart});
}

/// @nodoc
class __$$SkipCopyWithImpl<$Res>
    extends _$IQueryClauseCopyWithImpl<$Res, _$Skip>
    implements _$$SkipCopyWith<$Res> {
  __$$SkipCopyWithImpl(_$Skip _value, $Res Function(_$Skip) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? exclusivePlaceToStart = freezed,
  }) {
    return _then(_$Skip(
      exclusivePlaceToStart: freezed == exclusivePlaceToStart
          ? _value.exclusivePlaceToStart
          : exclusivePlaceToStart // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$Skip implements Skip {
  const _$Skip({required this.exclusivePlaceToStart, final String? $type})
      : $type = $type ?? 'skip';

  factory _$Skip.fromJson(Map<String, dynamic> json) => _$$SkipFromJson(json);

  @override
  final dynamic exclusivePlaceToStart;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IQueryClause.skip(exclusivePlaceToStart: $exclusivePlaceToStart)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Skip &&
            const DeepCollectionEquality()
                .equals(other.exclusivePlaceToStart, exclusivePlaceToStart));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(exclusivePlaceToStart));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SkipCopyWith<_$Skip> get copyWith =>
      __$$SkipCopyWithImpl<_$Skip>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int amount) limit,
    required TResult Function(dynamic exclusivePlaceToStart) skip,
    required TResult Function(SortQueryClauseOrder order, String field) sort,
    required TResult Function(IWhereComparator comparator, String field) where,
  }) {
    return skip(exclusivePlaceToStart);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int amount)? limit,
    TResult? Function(dynamic exclusivePlaceToStart)? skip,
    TResult? Function(SortQueryClauseOrder order, String field)? sort,
    TResult? Function(IWhereComparator comparator, String field)? where,
  }) {
    return skip?.call(exclusivePlaceToStart);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int amount)? limit,
    TResult Function(dynamic exclusivePlaceToStart)? skip,
    TResult Function(SortQueryClauseOrder order, String field)? sort,
    TResult Function(IWhereComparator comparator, String field)? where,
    required TResult orElse(),
  }) {
    if (skip != null) {
      return skip(exclusivePlaceToStart);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Limit value) limit,
    required TResult Function(Skip value) skip,
    required TResult Function(_Sort value) sort,
    required TResult Function(Where value) where,
  }) {
    return skip(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Limit value)? limit,
    TResult? Function(Skip value)? skip,
    TResult? Function(_Sort value)? sort,
    TResult? Function(Where value)? where,
  }) {
    return skip?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Limit value)? limit,
    TResult Function(Skip value)? skip,
    TResult Function(_Sort value)? sort,
    TResult Function(Where value)? where,
    required TResult orElse(),
  }) {
    if (skip != null) {
      return skip(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$SkipToJson(
      this,
    );
  }
}

abstract class Skip implements IQueryClause {
  const factory Skip({required final dynamic exclusivePlaceToStart}) = _$Skip;

  factory Skip.fromJson(Map<String, dynamic> json) = _$Skip.fromJson;

  dynamic get exclusivePlaceToStart;
  @JsonKey(ignore: true)
  _$$SkipCopyWith<_$Skip> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SortCopyWith<$Res> {
  factory _$$_SortCopyWith(_$_Sort value, $Res Function(_$_Sort) then) =
      __$$_SortCopyWithImpl<$Res>;
  @useResult
  $Res call({SortQueryClauseOrder order, String field});
}

/// @nodoc
class __$$_SortCopyWithImpl<$Res>
    extends _$IQueryClauseCopyWithImpl<$Res, _$_Sort>
    implements _$$_SortCopyWith<$Res> {
  __$$_SortCopyWithImpl(_$_Sort _value, $Res Function(_$_Sort) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? order = null,
    Object? field = null,
  }) {
    return _then(_$_Sort(
      order: null == order
          ? _value.order
          : order // ignore: cast_nullable_to_non_nullable
              as SortQueryClauseOrder,
      field: null == field
          ? _value.field
          : field // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Sort implements _Sort {
  const _$_Sort({required this.order, required this.field, final String? $type})
      : $type = $type ?? 'sort';

  factory _$_Sort.fromJson(Map<String, dynamic> json) => _$$_SortFromJson(json);

  @override
  final SortQueryClauseOrder order;
  @override
  final String field;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IQueryClause.sort(order: $order, field: $field)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Sort &&
            (identical(other.order, order) || other.order == order) &&
            (identical(other.field, field) || other.field == field));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, order, field);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SortCopyWith<_$_Sort> get copyWith =>
      __$$_SortCopyWithImpl<_$_Sort>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int amount) limit,
    required TResult Function(dynamic exclusivePlaceToStart) skip,
    required TResult Function(SortQueryClauseOrder order, String field) sort,
    required TResult Function(IWhereComparator comparator, String field) where,
  }) {
    return sort(order, field);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int amount)? limit,
    TResult? Function(dynamic exclusivePlaceToStart)? skip,
    TResult? Function(SortQueryClauseOrder order, String field)? sort,
    TResult? Function(IWhereComparator comparator, String field)? where,
  }) {
    return sort?.call(order, field);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int amount)? limit,
    TResult Function(dynamic exclusivePlaceToStart)? skip,
    TResult Function(SortQueryClauseOrder order, String field)? sort,
    TResult Function(IWhereComparator comparator, String field)? where,
    required TResult orElse(),
  }) {
    if (sort != null) {
      return sort(order, field);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Limit value) limit,
    required TResult Function(Skip value) skip,
    required TResult Function(_Sort value) sort,
    required TResult Function(Where value) where,
  }) {
    return sort(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Limit value)? limit,
    TResult? Function(Skip value)? skip,
    TResult? Function(_Sort value)? sort,
    TResult? Function(Where value)? where,
  }) {
    return sort?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Limit value)? limit,
    TResult Function(Skip value)? skip,
    TResult Function(_Sort value)? sort,
    TResult Function(Where value)? where,
    required TResult orElse(),
  }) {
    if (sort != null) {
      return sort(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_SortToJson(
      this,
    );
  }
}

abstract class _Sort implements IQueryClause {
  const factory _Sort(
      {required final SortQueryClauseOrder order,
      required final String field}) = _$_Sort;

  factory _Sort.fromJson(Map<String, dynamic> json) = _$_Sort.fromJson;

  SortQueryClauseOrder get order;
  String get field;
  @JsonKey(ignore: true)
  _$$_SortCopyWith<_$_Sort> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$WhereCopyWith<$Res> {
  factory _$$WhereCopyWith(_$Where value, $Res Function(_$Where) then) =
      __$$WhereCopyWithImpl<$Res>;
  @useResult
  $Res call({IWhereComparator comparator, String field});

  $IWhereComparatorCopyWith<$Res> get comparator;
}

/// @nodoc
class __$$WhereCopyWithImpl<$Res>
    extends _$IQueryClauseCopyWithImpl<$Res, _$Where>
    implements _$$WhereCopyWith<$Res> {
  __$$WhereCopyWithImpl(_$Where _value, $Res Function(_$Where) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? comparator = null,
    Object? field = null,
  }) {
    return _then(_$Where(
      comparator: null == comparator
          ? _value.comparator
          : comparator // ignore: cast_nullable_to_non_nullable
              as IWhereComparator,
      field: null == field
          ? _value.field
          : field // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $IWhereComparatorCopyWith<$Res> get comparator {
    return $IWhereComparatorCopyWith<$Res>(_value.comparator, (value) {
      return _then(_value.copyWith(comparator: value));
    });
  }
}

/// @nodoc
@JsonSerializable()
class _$Where implements Where {
  const _$Where(
      {required this.comparator, required this.field, final String? $type})
      : $type = $type ?? 'where';

  factory _$Where.fromJson(Map<String, dynamic> json) => _$$WhereFromJson(json);

  @override
  final IWhereComparator comparator;
  @override
  final String field;

  @JsonKey(name: 'runtimeType')
  final String $type;

  @override
  String toString() {
    return 'IQueryClause.where(comparator: $comparator, field: $field)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Where &&
            (identical(other.comparator, comparator) ||
                other.comparator == comparator) &&
            (identical(other.field, field) || other.field == field));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, comparator, field);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$WhereCopyWith<_$Where> get copyWith =>
      __$$WhereCopyWithImpl<_$Where>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int amount) limit,
    required TResult Function(dynamic exclusivePlaceToStart) skip,
    required TResult Function(SortQueryClauseOrder order, String field) sort,
    required TResult Function(IWhereComparator comparator, String field) where,
  }) {
    return where(comparator, field);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int amount)? limit,
    TResult? Function(dynamic exclusivePlaceToStart)? skip,
    TResult? Function(SortQueryClauseOrder order, String field)? sort,
    TResult? Function(IWhereComparator comparator, String field)? where,
  }) {
    return where?.call(comparator, field);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int amount)? limit,
    TResult Function(dynamic exclusivePlaceToStart)? skip,
    TResult Function(SortQueryClauseOrder order, String field)? sort,
    TResult Function(IWhereComparator comparator, String field)? where,
    required TResult orElse(),
  }) {
    if (where != null) {
      return where(comparator, field);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Limit value) limit,
    required TResult Function(Skip value) skip,
    required TResult Function(_Sort value) sort,
    required TResult Function(Where value) where,
  }) {
    return where(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Limit value)? limit,
    TResult? Function(Skip value)? skip,
    TResult? Function(_Sort value)? sort,
    TResult? Function(Where value)? where,
  }) {
    return where?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Limit value)? limit,
    TResult Function(Skip value)? skip,
    TResult Function(_Sort value)? sort,
    TResult Function(Where value)? where,
    required TResult orElse(),
  }) {
    if (where != null) {
      return where(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$WhereToJson(
      this,
    );
  }
}

abstract class Where implements IQueryClause {
  const factory Where(
      {required final IWhereComparator comparator,
      required final String field}) = _$Where;

  factory Where.fromJson(Map<String, dynamic> json) = _$Where.fromJson;

  IWhereComparator get comparator;
  String get field;
  @JsonKey(ignore: true)
  _$$WhereCopyWith<_$Where> get copyWith => throw _privateConstructorUsedError;
}
