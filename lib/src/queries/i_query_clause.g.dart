// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'i_query_clause.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Limit _$$_LimitFromJson(Map<String, dynamic> json) => _$_Limit(
      amount: json['amount'] as int,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_LimitToJson(_$_Limit instance) => <String, dynamic>{
      'amount': instance.amount,
      'runtimeType': instance.$type,
    };

_$Skip _$$SkipFromJson(Map<String, dynamic> json) => _$Skip(
      exclusivePlaceToStart: json['exclusivePlaceToStart'],
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$SkipToJson(_$Skip instance) => <String, dynamic>{
      'exclusivePlaceToStart': instance.exclusivePlaceToStart,
      'runtimeType': instance.$type,
    };

_$_Sort _$$_SortFromJson(Map<String, dynamic> json) => _$_Sort(
      order: $enumDecode(_$SortQueryClauseOrderEnumMap, json['order']),
      field: json['field'] as String,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$_SortToJson(_$_Sort instance) => <String, dynamic>{
      'order': _$SortQueryClauseOrderEnumMap[instance.order]!,
      'field': instance.field,
      'runtimeType': instance.$type,
    };

const _$SortQueryClauseOrderEnumMap = {
  SortQueryClauseOrder.asc: 'asc',
  SortQueryClauseOrder.desc: 'desc',
};

_$Where _$$WhereFromJson(Map<String, dynamic> json) => _$Where(
      comparator:
          IWhereComparator.fromJson(json['comparator'] as Map<String, dynamic>),
      field: json['field'] as String,
      $type: json['runtimeType'] as String?,
    );

Map<String, dynamic> _$$WhereToJson(_$Where instance) => <String, dynamic>{
      'comparator': instance.comparator,
      'field': instance.field,
      'runtimeType': instance.$type,
    };
