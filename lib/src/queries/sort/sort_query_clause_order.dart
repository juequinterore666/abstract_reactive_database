/// Abstraction over Sort query clause order.
enum SortQueryClauseOrder {
  /// Ascending sort order for query
  asc,

  /// Descending sort order for query
  desc,
}
