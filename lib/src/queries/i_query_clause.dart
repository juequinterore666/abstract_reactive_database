import 'package:freezed_annotation/freezed_annotation.dart';

import 'queries.dart';

part 'i_query_clause.freezed.dart';
part 'i_query_clause.g.dart';

/// Abstraction over database query options
@freezed
class IQueryClause with _$IQueryClause {
  /// Abstracts Limit clause in database query
  const factory IQueryClause.limit({required int amount}) = _Limit;

  /// Abstracts Skip clause in database query
  const factory IQueryClause.skip({required dynamic exclusivePlaceToStart}) =
      Skip;

  /// Abstracts Sort clause in database query
  const factory IQueryClause.sort(
      {required SortQueryClauseOrder order, required String field}) = _Sort;

  /// Abstracts Where clause in database query
  const factory IQueryClause.where(
      {required IWhereComparator comparator, required String field}) = Where;

  /// Create IQueryClause from json
  // coverage:ignore-start
  factory IQueryClause.fromJson(Map<String, dynamic> json) =>
      _$IQueryClauseFromJson(json);
  // coverage:ignore-end
}
