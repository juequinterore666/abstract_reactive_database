/// A type alias for a JSON object.
typedef JSON = Map<String, dynamic>;
