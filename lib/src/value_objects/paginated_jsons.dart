import 'package:abstract_reactive_database/src/types.dart';
import 'package:collection/collection.dart';

/// Class abstraction for allowing pagination.
class PaginatedJSONs {
  final Iterable<JSON> _documents;

  /// Object representing the query to be requested for retrieving next page.
  final Object? nextSkip;

  /// All the [Map<String, dynamic>] documents contained in current query page.
  Iterable<JSON> get documents => List<JSON>.unmodifiable(_documents);

  /// Constructor receiving [documents] and [nextSkip]
  PaginatedJSONs({required Iterable<JSON> documents, this.nextSkip})
      : _documents = documents;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PaginatedJSONs &&
          runtimeType == other.runtimeType &&
          nextSkip == other.nextSkip &&
          DeepCollectionEquality.unordered()
              .equals(_documents, other._documents);

  @override
  int get hashCode => _documents.hashCode ^ nextSkip.hashCode;
}
