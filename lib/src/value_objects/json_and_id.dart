import 'package:collection/collection.dart';

import '../types.dart';

/// A class that holds a JSON object and an ID for info to be written into the database.
class JSONAndId {
  /// Id to index the json in the database
  final String id;

  /// Info to be written into the database
  final JSON json;

  // ignore: public_member_api_docs
  JSONAndId({required this.id, required JSON json})
      : json = Map<String, dynamic>.of(json);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is JSONAndId &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          DeepCollectionEquality.unordered().equals(json, other.json);

  @override
  int get hashCode =>
      id.hashCode ^
      json.entries.fold(
          0,
          (int previousValue, MapEntry<String, dynamic> element) =>
              previousValue ^ element.key.hashCode ^ element.value.hashCode);

  // coverage:ignore-start
  @override
  String toString() {
    return 'JSONAndId{id: $id, json: $json}';
  }
  // coverage:ignore-end
}
