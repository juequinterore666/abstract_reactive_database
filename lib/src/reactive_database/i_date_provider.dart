/// Provides current date
abstract class IDateProvider {
  /// Returns current [DateTime]
  DateTime now();

  /// Returns current [DateTime] in milliseconds since epoch
  int millisecondsSinceEpoch();
}
