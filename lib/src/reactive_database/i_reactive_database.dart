import 'package:abstract_reactive_database/src/lib.dart';
import 'package:abstract_reactive_database/src/reactive_database/reactive_transaction_handler.dart';

/// Abstract class for modelling a reactive database.
///
/// It should return Streams from read-only methods that update each time the
/// read info changes in the source db.
abstract class IReactiveDatabase {
  /// Creates zero or more new registers in the database.
  ///
  /// New registers are created in [table] with the info contained in
  /// [JSONAndId.json] and it's indexed by [JSONAndId.id].
  Future<void> create({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  });

  /// Deletes zero or more registers in the database depending on the received
  /// ids.
  ///
  /// All registers with id in [ids] are deleted from [table].
  /// In case [ids] is empty it will not delete a single register in [table].
  Future<void> delete({
    required String table,
    required Iterable<String> ids,
  });

  /// Deletes zero or more registers in the database depending on some
  /// conditions.
  ///
  /// All registers matching [queryClauses] are deleted from [table].
  /// In case [queryClauses] is empty it will delete all registers in [table].
  Future<void> deleteWhere({
    required String table,
    required Iterable<IQueryClause> queryClauses,
  });

  /// Deletes [table] and its content from the database.
  Future<void> dropTable({
    required String table,
  });

  /// Reads zero or one register from the database.
  ///
  /// Register is read from [table] and it's matched by its [id].
  /// In case there's no matched register, returns null.
  Stream<JSON?> read({
    required String table,
    required String id,
  });

  /// Reads zero or more registers from the database.
  ///
  /// All registers matching [queryClauses] are read from [table]
  /// In case [queryClauses] is empty it will read all registers in [table].
  Stream<PaginatedJSONs> readWhere({
    required String table,
    Iterable<IQueryClause> queryClauses = const <IQueryClause>{},
  });

  /// Receives a [ReactiveTransactionHandler] to execute a transaction.
  Future<void> runTransaction(ReactiveTransactionHandler transactionHandler);

  /// Replaces zero or more registers in the database.
  ///
  /// Registers in [table] indexed by [JSONAndId.id] are replaced by data in
  /// [JSONAndId.json]
  Future<void> update({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  });

  /// Creates or patches received registers in the database.
  ///
  /// In case the register indexed by [JSONAndId.id] exists in [table] the
  /// register is updated with [JSONAndId.json] otherwise the register is
  /// created in [table], indexed by id field in [JSONAndId.id] and containing
  /// info from [JSONAndId.json]
  ///
  /// Example:
  /// Given a register like this: {id: '1', name: 'John', age: 30}
  /// If we call upsert(table, id: '1', json: {"deleted": true}) we will have at
  /// the end {id: '1', name: 'John', age: 30, deleted: true}
  ///
  /// If the register doesn't exist, it will be created with the info in the
  /// [JSONAndId.json]
  Future<void> upsert({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  });
}
