import 'package:abstract_reactive_database/abstract_reactive_database.dart';

/// A function that receives a [IReactiveDatabase] and executes all operations
/// inside in a single transaction, failing if an error is thrown
typedef ReactiveTransactionHandler = Future<void> Function(
    IReactiveDatabase transaction);
