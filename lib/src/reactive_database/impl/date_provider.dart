import '../../lib.dart';

/// [IDateProvider] implementation that uses [DateTime.now] and [DateTime.millisecondsSinceEpoch].
class DateProvider implements IDateProvider {
  @override
  DateTime now() => DateTime.now();

  @override
  int millisecondsSinceEpoch() => now().millisecondsSinceEpoch;
}
