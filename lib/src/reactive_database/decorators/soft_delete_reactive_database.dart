import '../../lib.dart';

/// Decorator to handle soft deletion of items in database.
///
/// It prevents actually deleting info from database by adding a boolean 'isDeleted' field to keep track of deleted items and not retrieve them in queries
class SoftDeleteReactiveDatabase implements IReactiveDatabase {
  static const String _isDeletedKey = "isDeleted";

  final IReactiveDatabase _reactiveDatabase;

  /// Constructor receiving [IReactiveDatabase] instance to be decorated
  SoftDeleteReactiveDatabase(this._reactiveDatabase);

  @override
  Future<void> create({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) {
    final Iterable<JSONAndId> mapstoUpsert =
        jsonsAndIds.map(_addIsDeletedFalse);
    return _reactiveDatabase.create(table: table, jsonsAndIds: mapstoUpsert);
  }

  @override
  Future<void> delete({
    required String table,
    required Iterable<String> ids,
  }) {
    final Iterable<JSONAndId> deletedJsonsWithIds = ids.map(
      (String id) =>
          JSONAndId(id: id, json: <String, dynamic>{_isDeletedKey: true}),
    );

    return _reactiveDatabase.upsert(
      table: table,
      jsonsAndIds: deletedJsonsWithIds,
    );
  }

  @override
  Future<void> deleteWhere({
    required String table,
    required Iterable<IQueryClause> queryClauses,
  }) async {
    final PaginatedJSONs paginatedDocumentsToDelete =
        await readWhere(table: table, queryClauses: queryClauses).first;

    final Iterable<JSONAndId> mapsToUpsert = paginatedDocumentsToDelete
        .documents
        .map((Map<String, dynamic> documentToDelete) =>
            JSONAndId(id: documentToDelete['id'], json: <String, dynamic>{}))
        .map(_addIsDeletedTrue);
    return _reactiveDatabase.upsert(table: table, jsonsAndIds: mapsToUpsert);
  }

  @override
  Future<void> dropTable({required String table}) {
    return _reactiveDatabase.dropTable(table: table);
  }

  @override
  Stream<Map<String, dynamic>?> read({
    required String table,
    required String id,
  }) {
    final IQueryClause idQueryClause = IQueryClause.where(
        comparator: IWhereComparator.equals(value: id), field: "id");
    final IQueryClause isDeletedQueryClause = _buildIsDeletedQuery();

    return _reactiveDatabase.readWhere(
        table: table,
        queryClauses: <IQueryClause>[
          idQueryClause,
          isDeletedQueryClause
        ]).map((PaginatedJSONs paginatedDocuments) =>
        paginatedDocuments.documents.isEmpty
            ? null
            : paginatedDocuments.documents.first);
  }

  @override
  Stream<PaginatedJSONs> readWhere({
    required String table,
    Iterable<IQueryClause> queryClauses = const <IQueryClause>{},
  }) {
    final IQueryClause isDeletedQuery = _buildIsDeletedQuery();

    final List<IQueryClause> listQueryClauses =
        queryClauses.toList(growable: true);
    final int lastWhereIndex = listQueryClauses
        .lastIndexWhere((IQueryClause element) => element is Where);

    listQueryClauses.insert(lastWhereIndex + 1, isDeletedQuery);

    return _reactiveDatabase.readWhere(
      table: table,
      queryClauses: listQueryClauses,
    );
  }

  @override
  Future<void> runTransaction(ReactiveTransactionHandler transactionHandler) {
    return _reactiveDatabase.runTransaction(transactionHandler);
  }

  @override
  Future<void> update({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) {
    final Iterable<JSONAndId> mapsToUpdate =
        jsonsAndIds.map((JSONAndId jsonAndId) => _addIsDeletedFalse(jsonAndId));
    return _reactiveDatabase.update(table: table, jsonsAndIds: mapsToUpdate);
  }

  @override
  Future<void> upsert({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) {
    final Iterable<JSONAndId> mapsToUpsert =
        jsonsAndIds.map(_addIsDeletedFalse);
    return _reactiveDatabase.upsert(table: table, jsonsAndIds: mapsToUpsert);
  }

  IQueryClause _buildIsDeletedQuery() => const IQueryClause.where(
      comparator: IWhereComparator.equals(value: false), field: _isDeletedKey);

  JSONAndId _addIsDeletedFalse(JSONAndId jsonAndId) {
    final Map<String, dynamic> modifiedDocument =
        Map<String, dynamic>.of(jsonAndId.json);
    modifiedDocument[_isDeletedKey] = false;

    return JSONAndId(id: jsonAndId.id, json: modifiedDocument);
  }

  JSONAndId _addIsDeletedTrue(JSONAndId jsonAndId) {
    final Map<String, dynamic> modifiedDocument =
        Map<String, dynamic>.of(jsonAndId.json);
    modifiedDocument[_isDeletedKey] = true;

    return JSONAndId(id: jsonAndId.id, json: modifiedDocument);
  }
}
