import 'package:rxdart/rxdart.dart';

import '../../lib.dart';

/// [IReactiveDatabase] implementation that caches queries to prevent having multiple instances of the same query [Stream]
class CachingReactiveDatabase implements IReactiveDatabase {
  final IReactiveDatabase _reactiveDatabase;

  /// Constructor that receives [IReactiveDatabase] instance to be decorated
  CachingReactiveDatabase(this._reactiveDatabase);

  final Map<String, BehaviorSubject<JSON?>> _singleBehaviorSubjects =
      <String, BehaviorSubject<Map<String, dynamic>?>>{};

  final Map<String, BehaviorSubject<PaginatedJSONs>> _listBehaviorSubjects =
      <String, BehaviorSubject<PaginatedJSONs>>{};

  @override
  Future<void> create({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) =>
      _reactiveDatabase.create(table: table, jsonsAndIds: jsonsAndIds);

  @override
  Future<void> delete({
    required String table,
    required Iterable<String> ids,
  }) =>
      _reactiveDatabase.delete(table: table, ids: ids);

  @override
  Future<void> deleteWhere({
    required String table,
    required Iterable<IQueryClause> queryClauses,
  }) async =>
      _reactiveDatabase.deleteWhere(table: table, queryClauses: queryClauses);

  @override
  Future<void> dropTable({
    required String table,
  }) =>
      _reactiveDatabase.dropTable(table: table);

  @override
  Stream<Map<String, dynamic>?> read({
    required String table,
    required String id,
  }) {
    final IQueryClause idQueryClause = IQueryClause.where(
        comparator: IWhereComparator.equals(value: id), field: "id");

    final String behaviorSubjectId =
        _buildQueryId(table, <IQueryClause>[idQueryClause]);

    if (_singleBehaviorSubjects.containsKey(behaviorSubjectId)) {
      return _singleBehaviorSubjects[behaviorSubjectId]!;
    }

    final BehaviorSubject<Map<String, dynamic>?> behaviorSubject =
        _convertModelStreamToBehaviorSubject(table: table, id: id);

    _singleBehaviorSubjects[behaviorSubjectId] = behaviorSubject;

    return behaviorSubject;
  }

  @override
  Stream<PaginatedJSONs> readWhere({
    required String table,
    Iterable<IQueryClause> queryClauses = const <IQueryClause>[],
  }) {
    final String behaviorSubjectId = _buildQueryId(table, queryClauses);

    if (_listBehaviorSubjects.containsKey(behaviorSubjectId)) {
      return _listBehaviorSubjects[behaviorSubjectId]!;
    }

    final BehaviorSubject<PaginatedJSONs> behaviorSubject =
        _convertPaginatedDtoStreamToBehaviorSubject(
            table: table, queryClauses: queryClauses);

    _listBehaviorSubjects[behaviorSubjectId] = behaviorSubject;
    return behaviorSubject;
  }

  @override
  Future<void> runTransaction(ReactiveTransactionHandler transactionHandler) {
    return _reactiveDatabase.runTransaction(transactionHandler);
  }

  @override
  Future<void> update({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) =>
      _reactiveDatabase.update(
        table: table,
        jsonsAndIds: jsonsAndIds,
      );

  @override
  Future<void> upsert({
    required String table,
    required Iterable<JSONAndId> jsonsAndIds,
  }) =>
      _reactiveDatabase.upsert(table: table, jsonsAndIds: jsonsAndIds);

  String _buildQueryId(String table, Iterable<IQueryClause> list) {
    final StringBuffer stringBuffer = StringBuffer(table)..writeAll(list, "_");
    return stringBuffer.toString();
  }

  BehaviorSubject<Map<String, dynamic>?> _convertModelStreamToBehaviorSubject(
      {required String table, required String id}) {
    final Stream<Map<String, dynamic>?> documentStream =
        _reactiveDatabase.read(table: table, id: id);

    final BehaviorSubject<Map<String, dynamic>?> behaviorSubject =
        BehaviorSubject<Map<String, dynamic>?>();

    documentStream.listen(behaviorSubject.add);

    return behaviorSubject;
  }

  BehaviorSubject<PaginatedJSONs> _convertPaginatedDtoStreamToBehaviorSubject(
      {required String table, required Iterable<IQueryClause> queryClauses}) {
    final Stream<PaginatedJSONs> documentStream =
        _reactiveDatabase.readWhere(table: table, queryClauses: queryClauses);

    final BehaviorSubject<PaginatedJSONs> behaviorSubject =
        BehaviorSubject<PaginatedJSONs>();

    documentStream.listen(behaviorSubject.add);

    return behaviorSubject;
  }
}
