import 'package:abstract_reactive_database/src/lib.dart';

/// Decorator to automatically handle creation, and update date fields in database items.
class WriteDateTrackerReactiveDatabase implements IReactiveDatabase {
  /// Default constructor
  WriteDateTrackerReactiveDatabase(this._database, this._dateProvider);

  final IReactiveDatabase _database;
  final IDateProvider _dateProvider;
  static const String _creationDateKey = 'creationDate';
  static const String _updateDateKey = 'updateDate';

  @override
  Future<void> create(
      {required String table, required Iterable<JSONAndId> jsonsAndIds}) {
    final List<JSONAndId> modifiedJsonsAndIds = jsonsAndIds
        .map(
          (JSONAndId e) => JSONAndId(
            id: e.id,
            json: Map<String, dynamic>.of(e.json)
              ..addAll(<String, dynamic>{
                _creationDateKey: _dateProvider.millisecondsSinceEpoch()
              }),
          ),
        )
        .toList();

    return _database.create(
      table: table,
      jsonsAndIds: modifiedJsonsAndIds,
    );
  }

  @override
  Future<void> delete({required String table, required Iterable<String> ids}) {
    return _database.delete(
      table: table,
      ids: ids,
    );
  }

  @override
  Future<void> deleteWhere(
      {required String table, required Iterable<IQueryClause> queryClauses}) {
    return _database.deleteWhere(
      table: table,
      queryClauses: queryClauses,
    );
  }

  @override
  Future<void> dropTable({required String table}) {
    return _database.dropTable(
      table: table,
    );
  }

  @override
  Stream<JSON?> read({required String table, required String id}) {
    return _database.read(
      table: table,
      id: id,
    );
  }

  @override
  Stream<PaginatedJSONs> readWhere(
      {required String table,
      Iterable<IQueryClause> queryClauses = const <IQueryClause>{}}) {
    return _database.readWhere(
      table: table,
      queryClauses: queryClauses,
    );
  }

  @override
  Future<void> runTransaction(ReactiveTransactionHandler transactionHandler) {
    return _database.runTransaction(transactionHandler);
  }

  @override
  Future<void> update(
      {required String table, required Iterable<JSONAndId> jsonsAndIds}) {
    final List<JSONAndId> modifiedJsonsAndIds = jsonsAndIds
        .map(
          (JSONAndId e) => JSONAndId(
            id: e.id,
            json: Map<String, dynamic>.of(e.json)
              ..addAll(<String, dynamic>{
                _updateDateKey: _dateProvider.millisecondsSinceEpoch()
              }),
          ),
        )
        .toList();

    return _database.update(
      table: table,
      jsonsAndIds: modifiedJsonsAndIds,
    );
  }

  @override
  Future<void> upsert(
      {required String table, required Iterable<JSONAndId> jsonsAndIds}) {
    final List<JSONAndId> modifiedJsonsAndIds = jsonsAndIds
        .map(
          (JSONAndId e) => JSONAndId(
            id: e.id,
            json: Map<String, dynamic>.of(e.json)
              ..addAll(<String, dynamic>{
                _creationDateKey: _dateProvider.millisecondsSinceEpoch(),
                _updateDateKey: _dateProvider.millisecondsSinceEpoch(),
              }),
          ),
        )
        .toList();

    return _database.upsert(
      table: table,
      jsonsAndIds: modifiedJsonsAndIds,
    );
  }
}
